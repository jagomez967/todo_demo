﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ToDoDataAccess.DataAccess;

namespace ToDoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private IHostingEnvironment _hostingEnvironment;

        public FileController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        [HttpPost("download")]
        public async Task<IActionResult> Post([FromBody] dynamic value)
        {
            int docID = 0;
            Guid docGUID = Guid.Empty;

            try
            { docID = int.Parse(value.documentID.Value.ToString()); }
            catch { }

            try { docGUID = new Guid(value.guid.Value); }
            catch { }

            if (docID == 0 && docGUID == Guid.Empty)
            {
                return new BadRequestResult();
            }

            string localFilePath = ToDoBuilder.GetFilePath(documentID: docID,documentGUID: docGUID);
            
            string fileName = Path.GetFileName(localFilePath);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            StreamContent stream = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));

            if (stream == null)
                return new NotFoundResult();
            return File(await stream.ReadAsByteArrayAsync(),"application/octet-stream", fileName);

        }

        [HttpPost("upload")]
        public ActionResult<Guid> Post([FromForm]IFormFile file)
        {
            string fullPath = string.Empty;
            string folderName = "tempFiles";
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName);
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            if (file.Length > 0)
            {
                string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                fullPath = Path.Combine(newPath, fileName);
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
            }
            return ToDoBuilder.RegisterFile(fullPath);
        }

        [HttpGet("name/{id}")]
        public ActionResult<string> Get(int id)
        {
            return Path.GetFileName(ToDoBuilder.GetFilePath(documentID: id));
        }
    }
}