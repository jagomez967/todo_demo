﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using ToDoDataAccess.DataAccess;
using ToDoDataAccess.Model;

namespace ToDoDataAccess.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<List<ToDo>> Get()
        {
            return ToDoBuilder.GetAll();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<ToDo> Get(int id)
        {
            return ToDoBuilder.Get(id);
        }

        [HttpGet("resolve/{id}")]
        public ActionResult<bool> Resolve(int id)
        {
            return ToDoBuilder.Resolve(id);
        }

        [HttpGet("search")]
        public ActionResult<List<ToDo>> Get(int id = 0, string descripcion = "", State estado = State.indefinido)
        {
            return ToDoBuilder.GetAll(id, descripcion, estado);
        }

        // POST api/values
        [HttpPost]
        public int Post([FromBody] dynamic value)
        {
            Guid docGUID;
            try
            {
                docGUID = new Guid(value.documentGUID.Value);
            }
            catch
            {
                docGUID = Guid.Empty;
            }             
            return ToDoBuilder.Add(descripcion: value.descripcion.Value, tempId: docGUID);
        }
    }
}
