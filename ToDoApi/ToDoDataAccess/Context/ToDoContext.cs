﻿using Microsoft.EntityFrameworkCore;
using ToDoDataAccess.Model;

namespace ToDoDataAccess.Context
{
    public class ToDoContext : DbContext
    {
        public ToDoContext(DbContextOptions<ToDoContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        public ToDoContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("ingrese string de conexion a la base de datos");
                base.OnConfiguring(optionsBuilder);
            }
        }

        public virtual DbSet<ToDo> Tasks { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<AuxDocument> AuxDocuments { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ToDo>();
            modelBuilder.Entity<Document>();
            modelBuilder.Entity<AuxDocument>();
        }
    }
}
