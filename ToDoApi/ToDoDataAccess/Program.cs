﻿using ToDoDataAccess.Context;
using ToDoDataAccess.Model;

namespace ToDoDataAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new ToDoContext())
            {
                var stud = new ToDo()
                {
                    Descripcion = "new description document",
                    Estado = State.Pendiente,
                    Document = new Document() { Path = "test/test/test" }
                };
                ctx.Database.EnsureCreated();
                ctx.Tasks.Add(stud);
                ctx.SaveChanges();
            }
        }
    }
}
