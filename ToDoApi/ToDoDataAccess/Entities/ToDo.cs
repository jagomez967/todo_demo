﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoDataAccess.Model
{
    public class ToDo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Descripcion { get; set; }
        public State Estado { get; set; }
        public int? DocumentID { get; set; }

        [ForeignKey(nameof(DocumentID))]
        public virtual Document Document { get; set; }
    }
}
