﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoDataAccess.Context;
using ToDoDataAccess.Model;

namespace ToDoDataAccess.DataAccess
{
    public static class ToDoBuilder
    {
        public static bool Resolve(int idTarea) {
            using (var context = new ToDoContext()) {
                var result = context.Tasks.SingleOrDefault(t => t.ID == idTarea);
                if (result != null)
                {
                    result.Estado = State.Resuelta;
                    context.SaveChanges();
                }
                return true;
            }

        }
        public static string GetFilePath(Guid documentGUID = default(Guid), int documentID = 0)
        {
            string path = string.Empty;
            using(var context = new ToDoContext())
            {
                if(documentGUID != Guid.Empty)
                {
                    path = context.AuxDocuments.FirstOrDefault(x => documentGUID == x.ID).Path;
                }
                if(documentID != 0)
                {
                    path = context.Documents.FirstOrDefault(d => documentID == d.ID).Path;
                }
            }
            return path;
        }
        public static Guid RegisterFile(string filePath)
        {
            AuxDocument doc = new AuxDocument() { Path = filePath};
            using(var context = new ToDoContext())
            {
                context.AuxDocuments.Add(doc);
                context.SaveChanges();
            }
            return doc.ID;
        }
        public static ToDo Get(int id)
        {
            ToDo toDoItem = new ToDo();
            using (var context = new ToDoContext())
            {
                toDoItem = context.Tasks.FirstOrDefault(t => t.ID == id);
            }
            return toDoItem;
        }
        public static List<ToDo> GetAll()
        {
            List<ToDo> items = new List<ToDo>();
            using (var context = new ToDoContext())
            {
                items = context.Tasks.ToList();
            }
            return items;
        }
        public static List<ToDo> GetAll(int id = 0, string descripcion = "", State estado = State.indefinido)
        {
            List<ToDo> items = new List<ToDo>();
            using (var context = new ToDoContext())
            {
                items = context.Tasks.Where(t => id == 0 || t.ID == id)
                                    .Where(t => descripcion == "" || t.Descripcion.Contains(descripcion))
                                    .Where(t => estado == State.indefinido || t.Estado == estado)
                                    .ToList();
            }
            return items;
        }
        public static int Add(string descripcion = "", Guid tempId = default(Guid))
        {
            ToDo item = new ToDo()
            {
                Descripcion = descripcion,
                Estado = State.Pendiente
            };

            using (var context = new ToDoContext())
            {
                //Si tiene GUID de documento la agrego al item ToDo
                if (tempId != Guid.Empty)
                {
                    var xDoc = context.AuxDocuments.FirstOrDefault(x => x.ID == tempId);
                    item.Document = new Document(xDoc.Path);
                    context.AuxDocuments.Remove(xDoc);
                }

                context.Tasks.Add(item);
                context.SaveChanges();
            }
            return item.ID;
        }
    }
}
