import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';  
import { Component, OnInit } from '@angular/core';
import { JSONP_ERR_WRONG_RESPONSE_TYPE } from '@angular/common/http/src/jsonp';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent implements OnInit {
  constructor(private httpService: HttpClient) { }  
  tasks: string[];  

  downLoadFile(data: Blob,name :string) {
    const url = window.URL.createObjectURL(data);
    const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;

    a.href = url;
    a.download = name;
    document.body.appendChild(a);
    a.click();

    document.body.removeChild(a);
    URL.revokeObjectURL(url);
}
  downloadDocument(documentId: number){
    //Primero obtengo el nombre y despues descargo    
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin':'*',
      'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'  ,      
      'Content-Type':'application/json'
    });
    
    const httpOptions = {
      responseType: 'blob' as 'json',
      headers: headers
    };
    let item = {
    documentID: documentId
    };
    let url = `https://localhost:44367/api/file/download`

    this.httpService.get(`https://localhost:44367/api/file/name/${documentId}`,
    {headers: {'Content-Type':'text/plain'},
    responseType: 'text'}).toPromise().then(  
      data => {  
            let filename = data as string;  
            this.httpService.post<Blob>(url,item,httpOptions)
                .toPromise().then(  
                data => this.downLoadFile(data,filename));  
            }            
      );
  };

  ngOnInit() {  
    this.httpService.get('https://localhost:44367/api/task').subscribe(  
      data => {  
       this.tasks = data as string [];  
      }  
    );  
  }  
}
