import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpEventType } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-to-do-add',
  templateUrl: './to-do-add.component.html',
  styleUrls: ['./to-do-add.component.css']
})


export class ToDoAddComponent implements OnInit {
  selectedFile : File = null;
  private toDoDescription: String;
  
  onUpload(){   
    //En el upload primero subo el archivo en el caso de que exista,
    //Si se subio bien entonces cargo la tarea.
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin':'*',
      'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'  ,
       'Accept': 'application/json'
    });  
  const httpOptions = {
    headers: headers
  };
    if(this.selectedFile)
    {
      let formData:FormData = new FormData();
      formData.append('', this.selectedFile,this.selectedFile.name);

      this.httpService.post(`https://localhost:44367/api/file/upload`, formData, 
            httpOptions).toPromise()
            .then( 
              data => this.uploadTask(data.toString()),
              error => console.log(error)
          );      
    }
    else{
      this.uploadTask();
    }    
  }
  
  uploadTask(documentGUID =''){
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin':'*',
      'Content-Type':'application/json',
      'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT'  
    });
    const httpOptions = {
      headers: headers
    };
    let item = {
    descripcion: this.toDoDescription,
    documentGUID: documentGUID
    };  

    const url ='https://localhost:44367/api/task';
    this.httpService.post(url,item,httpOptions)
    .subscribe(  
      data => {  
       console.log(data);  
      });
  }
  
  onFileSelected(event){
    this.selectedFile = <File>event.target.files[0];
  }
  constructor(private httpService: HttpClient) { }

  ngOnInit() {
  }


}
