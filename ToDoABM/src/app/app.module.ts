import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';  
 
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToDoListComponent } from './to-do-list/to-do-list.component';
import { ToDoAddComponent } from './to-do-add/to-do-add.component';
import { RouterModule, Routes } from '@angular/router';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ToDoAdminComponent } from './to-do-admin/to-do-admin.component';

const appRoutes: Routes = [
  { path: 'add', component: ToDoAddComponent },
  { path: 'list',component: ToDoListComponent },
  { path: 'admin',component: ToDoAdminComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    ToDoListComponent,
    ToDoAddComponent,
    ToDoAdminComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(router: Router) {
    
}
}
