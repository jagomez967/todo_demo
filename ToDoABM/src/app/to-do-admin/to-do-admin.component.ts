import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { formatNumber } from '@angular/common';

@Component({
  selector: 'app-to-do-admin',
  templateUrl: './to-do-admin.component.html',
  styleUrls: ['./to-do-admin.component.css']
})
export class ToDoAdminComponent implements OnInit {

  constructor(private httpService: HttpClient) { }  
  tasks: string[];
  ngOnInit() {  
    this.httpService.get('https://localhost:44367/api/task').subscribe(  
      data => {  
       this.tasks = data as string [];  
      }  
    );  
  }  
  resolveTask(taskId: number){
    let url : string = `https://localhost:44367/api/task/resolve/${taskId.toString()}`;
    
    this.httpService.get(url).subscribe(t => this.ngOnInit());                  
  }
  onSubmit(form:NgForm ) {
    let id : number= form.value.taskId;
    let descripcion : string = form.value.descriptionField;
    let url: string = 'https://localhost:44367/api/task/search?'
    
    if(form.value.taskId && !isNaN(id)){
      url += 'id=' + id.toString() + '&';
    }
    if(descripcion && descripcion != ''){
      url += 'descripcion=' + descripcion + '&';
    }
    if(form.value.pendienteOk && form.value.pendienteOk === true){
      url += 'estado=0';
    }else if(form.value.resueltoOk && form.value.resueltoOk === true){
      url += 'estado=1';
    }else if(form.value.resueltoOk === form.value.pendienteOk){
      url += 'estado=2';
    }

    this.tasks= null ; 
    this.httpService.get(url).subscribe(  
      data => {  
       this.tasks = data as string [];  
      }  
    );
   }

}
