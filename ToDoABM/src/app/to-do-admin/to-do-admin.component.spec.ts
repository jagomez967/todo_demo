import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToDoAdminComponent } from './to-do-admin.component';

describe('ToDoAdminComponent', () => {
  let component: ToDoAdminComponent;
  let fixture: ComponentFixture<ToDoAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToDoAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToDoAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
